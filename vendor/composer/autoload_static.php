<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInitbc31fd04fd10ca52adca5c6b18d99d3f
{
    public static $prefixLengthsPsr4 = array (
        'D' => 
        array (
            'Dumexx\\Calculator\\' => 18,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Dumexx\\Calculator\\' => 
        array (
            0 => __DIR__ . '/../..' . '/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInitbc31fd04fd10ca52adca5c6b18d99d3f::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInitbc31fd04fd10ca52adca5c6b18d99d3f::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
